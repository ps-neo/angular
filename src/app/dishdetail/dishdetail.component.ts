import { Component, OnInit,Input , Inject} from '@angular/core';
import { Dish }  from  '../shared/dish';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DishService } from '../services/dish.service';
import 'rxjs/add/operator/switchMap';

import { FormBuilder, FormGroup, Validators} from '@angular/forms'; 
import { comment } from '../shared/comment';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.css'],
  providers:[]
})

export class DishdetailComponent implements OnInit {
  @Input()
  dish:Dish;

  commentForm: FormGroup;
  comment: comment;

  dishIds: number[];
  prev: number;
  next: number;

  formErrors={
    'author':'',
    'rating':'',
    'comment':''
  };
  validationMessages={
    'author':{
      'required': ' Name is required.',
      'minlength': ' Name must be atleast 2 characters long',
      'maxlength':' Name cannot be greater than 25 characters long'
    },
    'comment':{
      'required': 'Comment is required.',
    }
  };
  constructor(private location: Location,
        private fb: FormBuilder,
        private dishservice: DishService,
        @Inject('BaseURL') private BaseURL
        /*private route: ActivatedRoute*/) {
          this.createForm();
          console.log('Dish coming from Menu Componenet '+this.dish);
         }

  ngOnInit() {
  /*  this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
    this.route.paramMap
      .switchMap((params: Params) => this.dishservice.getDish(+params['id']))
      .subscribe(dish => { this.dish = dish; this.setPrevNext(dish.id); });
  */}
setPrevNext(dishId: number) {
    let index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1)%this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1)%this.dishIds.length];
  }

  createForm(){
    this.commentForm= this.fb.group({
      author:['',[Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating:5,
      comment:['',[Validators.required]]
    });

    this.commentForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any){
    if(!this.commentForm){ return; }
    const form = this.commentForm;

    for(const field in this.formErrors){
        this.formErrors[field]='';
        const control = form.get(field);
        if(control && control.dirty && !control.valid){
           const messages= this.validationMessages[field];
           for(const key in control.errors){
             this.formErrors[field] += messages[key]+ ' ';
             
           }
        }
    }
  }
  onSubmit(){
    this.comment=  this.commentForm.value;
    console.log(this.comment);
    console.log(this.dish)
// Return a Date object as a String, using the ISO standard:    
    var d = new Date();
    var n = d.toISOString();
    this.comment.date=n;
    this.dish.comment.push(this.comment);
    this.commentForm.reset({
      author:'',
      rating:'',
      comment:''
    });
  }

}
