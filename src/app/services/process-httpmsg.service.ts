import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import { Http, Response } from '@angular/http';

import { baseURL } from '../shared/baseurl';


@Injectable()
export class ProcessHttpmsgService {

  constructor() { }

  public extractData(res: Response){
    let body = res.json();

    return body || { };
  }
}
