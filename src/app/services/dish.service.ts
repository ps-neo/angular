import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';

// Removing this import as DISHES will be fetched from jseon-server as of now
// And in Real environment it will be coming from server.
//import { DISHES } from '../shared/dishes'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/of';
import { ProcessHttpmsgService } from './process-httpmsg.service';

//import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Http } from '@angular/http';
import { baseURL } from '../shared/baseurl';
import { RestangularModule, Restangular } from 'ngx-restangular';
import { RestangularConfigFactory } from  '../shared/restConfig'; 

@Injectable()
export class DishService {

  constructor(private processHttpmsgService:ProcessHttpmsgService,
              private http: Http,
              private restangular: Restangular) { }
 /*
getDishes(): Dish[]{
     return DISHES;}
getDish(id: number): Dish{
   return DISHES.filter((dish) =>(dish.id === id) )[0];}
getFeaturedDish() :Dish{
   return DISHES.filter((dish) => dish.featured)[0];}
------------------(Using Promises)---------------------------------------
getDishes(): Promise<Dish[]>{
    return Promise.resolve(DISHES);
   }
getDish(id: number): Promise<Dish>{
    return Promise.resolve(DISHES.filter((dish) =>(dish.id === id) )[0]);
   }
getFeaturedDish() :Promise<Dish>{
   return Promise.resolve(DISHES.filter((dish) => dish.featured)[0]);
   }
=================(Using Promises with delay)=======================================================
getDishes(): Promise<Dish[]>{
+    return new Promise(resolve =>{
+      // Simulate server latency with 2 second delay
+      setTimeout(() => {resolve(DISHES)},2000)
  });
   }
   getDish(id: number): Promise<Dish>{
+    return new Promise(resolve =>{
+      // Simulate server latency with 2 second delay
+      setTimeout(() => {resolve(DISHES.filter((dish) =>(dish.id === id) )[0])},2000)
+    });
   }
   getFeaturedDish() :Promise<Dish>{
+    return new Promise(resolve => {
+      setTimeout(() => {resolve(DISHES.filter((dish) => dish.featured)[0])},2000)
+    });
   }   
------------------(Using Observable with delay and promise: can return only one value)----------------------------------   

  getDishes(): Promise<Dish[]>{
    return  Observable.of(DISHES).delay(2000).toPromise();
  }
  getDish(id: number): Promise<Dish>{
    return Observable.of(DISHES.filter((dish) =>(dish.id === id) )[0]).delay(2000).toPromise();
 //   return Observable.of(DISHES.filter((dish) => (dish.id === id))[0]).delay(2000).toPromise();
  }
  getFeaturedDish() :Promise<Dish>{
    return Observable.of(DISHES.filter((dish) => dish.featured)[0]).delay(2000).toPromise();
   }
=========================(Using Observable as return valuewith delay )===========================
getDishes(): Observable<Dish[]>{
 return  Observable.of(DISHES).delay(2000);
}
getDish(id: number): Observable<Dish>{
 return Observable.of(DISHES.filter((dish) =>(dish.id === id) )[0]).delay(2000);
}
getFeaturedDish() :Observable<Dish>{
  return Observable.of(DISHES.filter((dish) => dish.featured)[0]).delay(2000);
}
getDishIds(): Observable<number[]>{
    return Observable.of(DISHES.map( (dish) => dish.id )).delay(3000);
  }

=====================(Using only Observsbles )========================================
getDishes(): Observable<Dish[]>{
  return  this.http.get(baseURL + 'dishes')
                    .map(res => { return this.processHttpmsgService.extractData(res);}); 
}
getDish(id: number): Observable<Dish>{
  return this.http.get(baseURL + 'dishes/'+id)
                  .map(res => { return this.processHttpmsgService.extractData(res); });
}
getFeaturedDish() :Observable<Dish>{
  return   this.http.get(baseURL + 'dishes?featured=true')
               .map( res => { return this.processHttpmsgService.extractData(res)[0];});
  }
  getDishIds(): Observable<number[]>{
    return this.getDishes().
                            map(dishes => {return dishes.map( (dish) => dish.id )});
  }

*/
      // Using Restangular  //

getDishes(): Observable<Dish[]>{
  return  this.restangular.all('dishes').getList(); // getList() => returns an array of dishes
}

getDish(id: number): Observable<Dish>{
  return this.restangular.one('dishes', id).get(); // get() => for returning only one item
}
getFeaturedDish() :Observable<Dish>{
  return   this.restangular.all('dishes').getList({featured:true})
               .map(dishes => dishes[0] );
  }
  getDishIds(): Observable<number[]>{
    return this.getDishes().
                            map(dishes => {return dishes.map( (dish) => dish.id )});
  }

}
